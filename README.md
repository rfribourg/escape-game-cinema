# Escape Game Cinema

Cet Escape Game est un jeu en réalité virtuelle développé pour utilisation avec un casque Oculus Quest 2.

Ce jeu a été développé dans le cadre d'un projet étudiant sur plusieurs années ayant démarré en 2023, les étudiants chargés du projet changeant chaque année sans recouvrement des équipes. Chaque année, la nouvelle équipe est chargée de mettre à jour la plateforme, continuer le développement des fonctionnalités déjà introduites et de faire évoluer le jeu.

**1. Scénario**

Le monde du cinéma est en danger ! Dans chacune des bobines, les héros sont en grande difficulté : ils risquent de ne pas pouvoir terminer leur mission avant la fin du film ! Pour les aider, vous devez voyager de monde en monde pour récupérer les éléments qui sont dans le mauvais film.
La première étape est d'aider Rémy, le rat de Ratatouille, à réaliser sa fameuse recette pour récupérer *** (élément à choisir en fonction du prochain film que choisira d'implémenter l'équipe de l'année prochaine).

**2. Documentation**

Lien vers la documentation : https://gitlab.com/nlorioux/escape-game-cinema/-/wikis/WIKI-Homepage-%7C-Escape-Game-Cinema

**3. Spécifications techniques**

- L'application a été développée avec la dernière version LTS de Unity à la date de création du projet, c'est-à-dire Unity 2021.3.17f1.
- La pipeline de rendu graphique choisie est l'Universal Render Pipeline.

**4. À l'intention des successeurs du projet**

Au cours de nos réflexions, nous avons listé quelques idées de films qui pourraient mériter un niveau dans notre escape game. À vous de voir si vous décidez de les utiliser.

- Batman -> Entrepôt : caisses, objects … 

- Interstellar -> Chambre de Murphy : livres, poussière, … / Vaisseau spatial 

- Jurassic Park -> Labo (avec embryon de dinosaure) : chimie, biologie, …

- Titanic -> Cabine qui prend l’eau : cartes, argent, clé, hache …

- Matrix -> Pièce avec les pilules : pilules, objets randoms

- Forrest Gump : Banc : boite de chocolat 

- La La Land -> Embouteillage : gens dans leurs voitures, klaxon, …

- Iron Man -> Cave avec l’armure : bout de métal (objectif: reformer l’armure)

- Toy Story -> Chambre d’Andy : jouets

- Inception -> Ville pliée : toupie, rêves, …

- Les Schtroumpfs -> Chez Gargamel : cage, objets random 

- Harry Potter -> Bureau de Dumbledore : trucs magiques

Problème éventuel : Pour l’utilisation d’un certain shader (Volumetric Light), nous avons utilisé la Universal Render Pipeline. Il en résulte que si les paramètres ne sont pas totalement mis à jour, les matériaux peuvent apparaître en rose. Dans ce cas là, sélectionnez les matériaux roses, puis allez dans Edit > Rendering > Materials > Convert Selected Built-in Materials to URP.

**5. Les Controllers**
- La téléportation : on a une contrainte sur la zone de jeu (salle de cours) pour se déplacer dans une salle virtuelle grande on a choisi de téléporter le joueur .Ce dernier pour activer la téléportation dans sa main gauche  en cliquant  sur Axis1D.PrimaryHandTrigger .Un cylindre (Reticale) s’affiche lors de l’activation de la téléportation au bout du raycast pour indiquer la position de téléportation on a deux cas : 
    * Le rayon est en rouge si le Reticale n’est pas dans une zone de téléportation.Dans ce cas la position du joueur n’est pas changée.
    * Le rayon est en blanc ,le Reticale est donc dans une zone de téléportation et le joueur se téléporte vers la position de ce dernier.

Pour définir la zone de téléportation ,il faut ajouter le script TeleportationArea à la zone concernée.

- Grab les objets : pour prendre les objets on utilise la main droite .Si on rapproche à un grabableObject on peut le récupérer en cliquant sur le bouton Axis1D.SecondaryHandTrigger .Autant qu’on presse ce bouton l’objet est attaché à la main du joueur.
Pour définir un grabableObject  il faut ajouter à l’objet considéré les composants suivants (On laisse les valeurs par défauts il n'y a rien à modifier): 
        - BoxCllider 
        - RigideBody
        - XRGrabInteractable


