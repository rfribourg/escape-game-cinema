using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class OpenLocker : MonoBehaviour
{
    public GameObject Lock;
    public GameObject Drawer;
    public bool isLocked = true;
    public GameObject script;
    private Vector3 Move = new Vector3(0, 0.02f, 0);
    // Start is called before the first frame update
    void Start()
    {
        Drawer.GetComponent<Rigidbody>().isKinematic = true;
        Drawer.GetComponent<XRGrabInteractable>().enabled = false;


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Key"&&isLocked)
        {
            
            other.gameObject.transform.SetParent(transform.parent);
            this.gameObject.transform.SetPositionAndRotation(transform.position-Move, transform.rotation);
            this.gameObject.transform.SetParent(Drawer.transform);
            Lock.gameObject.transform.SetParent(Drawer.transform);
            this.GetComponent<Collider>().isTrigger = false;
            isLocked= false;
            Drawer.GetComponent<Rigidbody>().isKinematic = false;
            Drawer.GetComponent<XRGrabInteractable>().enabled = true;
            //Drawer.GetComponent<XRGrabInteractable>().enabled = true;
        }
    }
}
