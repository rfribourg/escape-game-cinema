using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Locomotioncontroller1 : MonoBehaviour
{

    public GameObject LeftTeleportation;
    public GameObject Rectile;

    public InputHelpers.Button teleportActivationButton;


    // Update is called once per frame
    void Update()
    {
        XRRayInteractor rayInteractor = LeftTeleportation.GetComponent<XRRayInteractor>();
        rayInteractor.enabled=CheckIfActivated();
        Rectile.SetActive(CheckIfActivated());
    }


    public bool CheckIfActivated()
    {
        var leftHandDevices = new List<UnityEngine.XR.InputDevice>();
        UnityEngine.XR.InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.LeftHand, leftHandDevices);
        bool isActive = false;
        if (leftHandDevices.Count == 1)
        {
            UnityEngine.XR.InputDevice device = leftHandDevices[0];
            if (device.TryGetFeatureValue(UnityEngine.XR.CommonUsages.gripButton, out bool triggerValue) && triggerValue)
            {
                isActive = true;
                
            }
            else
            {
                isActive = false;
            }
        }
       
        return isActive;
    }
}