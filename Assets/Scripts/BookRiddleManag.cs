using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.XR.Interaction.Toolkit;


public class BookRiddleManag : MonoBehaviour
{
    private bool isOpened = false;
    private bool isIn = false;
    public TextMeshPro text;
    public TMP_FontAsset f;
    public GameObject openedBook;
    //private bool close;

    //private bool col;
    private string nom = "";
    private bool isAllFixed;
    public GameObject book;
    public GameObject book_open2;
    public GameObject texte;
    public GameObject Part4;
    public GameObject Part41;
    public GameObject TextPart4;
    public GameObject TextPart41;
    public GameObject Part3;
    public GameObject Part31;
    public GameObject TextPart3;
    public GameObject TextPart31;
    public GameObject Part2;
    public GameObject Part21;
    public GameObject TextPart2;
    public GameObject TextPart21;

    void OnTriggerEnter(Collider other)
    {
        if (tag == "panel")
        {
            if (other.name == "Part2-1")
            {
                Part2.GetComponent<MeshRenderer>().enabled = true;
                Part21.GetComponent<MeshRenderer>().enabled = false;
                TextPart2.GetComponent<MeshRenderer>().enabled = true;
                TextPart21.GetComponent<MeshRenderer>().enabled = false;

            }

            if (other.name == "Part3-1")
            {
                Part3.GetComponent<MeshRenderer>().enabled = true;
                Part31.GetComponent<MeshRenderer>().enabled = false;
                TextPart3.GetComponent<MeshRenderer>().enabled = true;
                TextPart31.GetComponent<MeshRenderer>().enabled = false;
            }
            if (other.name == "Part4-1")
            {
                Part4.GetComponent<MeshRenderer>().enabled = true;
                Part41.GetComponent<MeshRenderer>().enabled = false;
                TextPart4.GetComponent<MeshRenderer>().enabled = true;
                TextPart41.GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }
    

    // Start is called before the first frame update
    void Start()
    {
        isAllFixed = false;
    }


    // Update is called once per frame
    void Update()
    {

        if (Part2.GetComponent<MeshRenderer>().enabled && Part3.GetComponent<MeshRenderer>().enabled && Part4.GetComponent<MeshRenderer>().enabled)
        {
            isAllFixed = true;
        }
        
        // tant que le livre est ouvert et qu'on est dans les interractions avec lui
        if (openedBook.GetComponent<MeshRenderer>().enabled)
        {
            //�a permet d'afficher le livre en mode d�cod�
            if (isAllFixed)
            {
                text.font = f;
            }
        }
    }
}
