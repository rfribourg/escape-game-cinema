using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookOpening : MonoBehaviour
{
    public GameObject openedBook;
    public GameObject bookText;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "hand")
        {
            openedBook.GetComponent<MeshRenderer>().enabled = true;
            bookText.GetComponent<MeshRenderer>().enabled = true;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
